package com.lzj.gen.jetbrains;

import sun.security.rsa.RSAPadding;
import sun.security.util.DerOutputStream;
import sun.security.util.DerValue;
import sun.security.x509.AlgorithmId;

import java.io.ByteArrayInputStream;
import java.io.FileWriter;
import java.io.IOException;
import java.math.BigInteger;
import java.security.InvalidAlgorithmParameterException;
import java.security.InvalidKeyException;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.security.cert.CertificateException;
import java.security.cert.CertificateFactory;
import java.security.cert.X509Certificate;
import java.security.interfaces.RSAPublicKey;

import javax.crypto.BadPaddingException;

/**
 * Created by lzj on 2023/11/24
 */
public class PowerConfigUtil {

    //jetbrains 内置public key
    private static final String JET_PRIVATE_KEY =
            "860106576952879101192782278876319243486072481962999610484027161162448933268423045647258145695082284265933019120714643752088997312766689988016808929265129401027490891810902278465065056686129972085119605237470899952751915070244375173428976413406363879128531449407795115913715863867259163957682164040613505040314747660800424242248055421184038777878268502955477482203711835548014501087778959157112423823275878824729132393281517778742463067583320091009916141454657614089600126948087954465055321987012989937065785013284988096504657892738536613208311013047138019418152103262155848541574327484510025594166239784429845180875774012229784878903603491426732347994359380330103328705981064044872334790365894924494923595382470094461546336020961505275530597716457288511366082299255537762891238136381924520749228412559219346777184174219999640906007205260040707839706131662149325151230558316068068139406816080119906833578907759960298749494098180107991752250725928647349597506532778539709852254478061194098069801549845163358315116260915270480057699929968468068015735162890213859113563672040630687357054902747438421559817252127187138838514773245413540030800888215961904267348727206110582505606182944023582459006406137831940959195566364811905585377246353";

    public static void genPowerPluginConfig(String certFile, String baseDir)
        throws CertificateException, NoSuchAlgorithmException, IOException, InvalidAlgorithmParameterException,
            BadPaddingException, InvalidKeyException {
        String certContent = PemUtil.readPemFullStr(certFile);
        CertificateFactory certFactory = CertificateFactory.getInstance("X.509");
        X509Certificate x509Certificate = (X509Certificate) certFactory.generateCertificate(
                new ByteArrayInputStream(certContent.getBytes()));
        RSAPublicKey publicKey = ((RSAPublicKey)x509Certificate.getPublicKey());
        String exponent = publicKey.getPublicExponent().toString(10);
        byte[] bytes = encodeSignature(x509Certificate.getTBSCertificate(), publicKey.getModulus().bitLength());
        String powerConfig = "[Result]\nEQUAL," + new BigInteger(1, x509Certificate.getSignature()).toString(10) + "," + exponent +
                "," + JET_PRIVATE_KEY + "->" +  new BigInteger(1, bytes).toString(10);
        FileWriter fileWriter = new FileWriter(baseDir + "/power.conf");
        fileWriter.write(powerConfig);
        fileWriter.close();
    }

    private static byte[] encodeSignature(byte[] values, int keySize)
        throws NoSuchAlgorithmException, InvalidAlgorithmParameterException, InvalidKeyException, IOException,
            BadPaddingException {
        MessageDigest messageDigest = MessageDigest.getInstance("SHA-256");
        messageDigest.update(values);
        byte[] bytes = messageDigest.digest();
        RSAPadding padding = RSAPadding.getInstance(RSAPadding.PAD_BLOCKTYPE_1, (keySize + 7) >> 3, null);
        DerOutputStream out = new DerOutputStream();
        new AlgorithmId(AlgorithmId.SHA256_oid).encode(out);
        out.putOctetString(bytes);
        DerValue result = new DerValue(DerValue.tag_Sequence, out.toByteArray());
        return padding.pad(result.toByteArray());
    }

}
