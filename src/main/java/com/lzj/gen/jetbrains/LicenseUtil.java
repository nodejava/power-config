package com.lzj.gen.jetbrains;

import com.alibaba.fastjson.JSONObject;

import org.apache.commons.lang3.RandomStringUtils;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by lzj on 2023/11/24
 */
public class LicenseUtil {

    //idea clion phpstorm goland pycharm webstorm webide rider datagrip rubymine appcode
    // dataspell gateway jetbrains_client jetbrainsclient studio devecostudio
    public enum Product {
        II("Idea"),
        CL("Clion"),
        PS("PhpStorm"),
        GO("Goland"),
        PC("Pycharm"),
        WS("Webstorm"),
        RD("Rider"),
        DB("Datagrip"),
        RM("Rubymine"),
        AC("Appcode"),
        DS("Dataspell");

        private final String name;
        Product(String name) {
            this.name = name;
        }

        public String getName() {
            return name;
        }
    }

    private static class ProductAuth {
        private String code;
        private String fallbackDate;
        private String paidUpTo;
        private boolean extend;

        public String getCode() {
            return code;
        }

        public void setCode(String code) {
            this.code = code;
        }

        public String getFallbackDate() {
            return fallbackDate;
        }

        public void setFallbackDate(String fallbackDate) {
            this.fallbackDate = fallbackDate;
        }

        public String getPaidUpTo() {
            return paidUpTo;
        }

        public void setPaidUpTo(String paidUpTo) {
            this.paidUpTo = paidUpTo;
        }

        public boolean isExtend() {
            return extend;
        }

        public void setExtend(boolean extend) {
            this.extend = extend;
        }
    }

    private static class License {
        private String licenseId;
        private String licenseeName;
        private String assigneeName;
        private String assigneeEmail;
        private String licenseRestriction;
        private boolean checkConcurrentUse;
        private List<ProductAuth> products;
        private String metadata;
        private String hash;
        private int gracePeriodDays;
        private boolean autoProlongated;
        private boolean isAutoProlongated;

        public License() {
            this.metadata = "0120230914PSAX000005";
            this.hash = "TRIAL:-1635216578";
            this.gracePeriodDays = 7;
            this.assigneeName = "";
            this.assigneeEmail = "";
            this.licenseRestriction = "";
        }

        public String getLicenseId() {
            return licenseId;
        }

        public void setLicenseId(String licenseId) {
            this.licenseId = licenseId;
        }

        public String getLicenseeName() {
            return licenseeName;
        }

        public void setLicenseeName(String licenseeName) {
            this.licenseeName = licenseeName;
        }

        public String getAssigneeName() {
            return assigneeName;
        }

        public void setAssigneeName(String assigneeName) {
            this.assigneeName = assigneeName;
        }

        public String getAssigneeEmail() {
            return assigneeEmail;
        }

        public void setAssigneeEmail(String assigneeEmail) {
            this.assigneeEmail = assigneeEmail;
        }

        public String getLicenseRestriction() {
            return licenseRestriction;
        }

        public void setLicenseRestriction(String licenseRestriction) {
            this.licenseRestriction = licenseRestriction;
        }

        public boolean isCheckConcurrentUse() {
            return checkConcurrentUse;
        }

        public void setCheckConcurrentUse(boolean checkConcurrentUse) {
            this.checkConcurrentUse = checkConcurrentUse;
        }

        public List<ProductAuth> getProducts() {
            return products;
        }

        public void setProducts(List<ProductAuth> products) {
            this.products = products;
        }

        public String getMetadata() {
            return metadata;
        }

        public void setMetadata(String metadata) {
            this.metadata = metadata;
        }

        public String getHash() {
            return hash;
        }

        public void setHash(String hash) {
            this.hash = hash;
        }

        public int getGracePeriodDays() {
            return gracePeriodDays;
        }

        public void setGracePeriodDays(int gracePeriodDays) {
            this.gracePeriodDays = gracePeriodDays;
        }

        public boolean isAutoProlongated() {
            return autoProlongated;
        }

        public void setAutoProlongated(boolean autoProlongated) {
            this.autoProlongated = autoProlongated;
        }
    }

    public static String genLicenseForAll(String licenseeName, String expireDate) {
        return genLicense(licenseeName, expireDate, Product.values());
    }

    public static String genLicense(String licenseeName, String expireDate, Product... products) {
        License license = new License();
        license.setLicenseId(RandomStringUtils.randomAlphabetic(10).toUpperCase());
        license.setLicenseeName(licenseeName);
        List<ProductAuth> productAuths = new ArrayList<>();
        for (Product product : products) {
            ProductAuth productAuth = new ProductAuth();
            productAuth.setCode(product.name());
            productAuth.setExtend(true);
            productAuth.setFallbackDate(expireDate);
            productAuth.setPaidUpTo(expireDate);
            productAuths.add(productAuth);
        }
        license.setProducts(productAuths);
        return JSONObject.toJSONString(license);
    }
}
